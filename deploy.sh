#!/bin/bash

# (c) 2019, Jon Johnson 
# released under GPLv3, see LICENSE.txt

REMOTE_USER="user"
REMOTE_HOST="domain.tld"
REMOTE_DIR="/var/www/sitename"
REMOTE_PORT=22
DEPLOY_DIR="deploy"
BACKUP_FILE_NAME="sitename.tar.gz"
BUILD_DIR="build"

# default options
debug=0
build=0
mount=1
backup=1
clear_deploy_dir=0
deploy=1
unmount=1
speak=0;

error() {
  echo "$@" >> /dev/stderr
}

fail() {
  error_code=$1
  shift
  error $@
  exit $error_code
}

while [[ $# -gt 0 ]]
do
  key="$1"
  case $key in
    --debug)
    debug=1
    speak=1
    shift
    ;;
    -b|--build)
    build=1
    shift
    ;;
    -nb|--no-build)
    build=0
    shift
    ;;
    -m|--mount)
    mount=1
    shift
    ;;
    -nm|--no-mount)
    mount=0
    shift
    ;;
    -p|--backup)
    backup=1
    shift
    ;;
    -np|--no-backup)
    backup=0
    shift
    ;;
    -c|--clear)
    clear_deploy_dir=1
    shift
    ;;
    -nc|--no-clear)
    clear_deploy_dir=0
    shift
    ;;
    -d|--deploy)
    deploy=1
    shift
    ;;
    -nd|--no-deploy)
    deploy=0
    shift
    ;;
    -u|--unmount)
    unmount=1
    shift
    ;;
    -nu|--no-unmount)
    unmount=0
    shift
    ;;
    --silent)
    speak=0;
    shift
    ;;
  esac
done

test_deploy_dir() {
  if [[ ! -d $DEPLOY_DIR ]]; then
     fail 2 "deploy dir $DEPLOY_DIR not present"
  fi
}

mount () {
  if [[ -n "$(ls -A $DEPLOY_DIR)" ]]; then
    fail 1 "deploy directory not ready"
  else 
    echo "mounting remote directory to $DEPLOY_DIR"
    remote="$REMOTE_USER@$REMOTE_HOST:$REMOTE_DIR"
    sshfs -p $REMOTE_PORT $remote $DEPLOY_DIR || fail 2 "could not mount remote dir $remote to local dir $DEPLOY_DIR"
  fi
}

backup () {
  test_deploy_dir
  if [[ -z "$(ls -A $DEPLOY_DIR)" ]]; then 
    echo "deploy dir empty. nothing to back up" && return 1 
  fi
  echo "backing up remote content"
  tar czf $BACKUP_FILE_NAME $DEPLOY_DIR || fail 4 "failed to backup site"
}

clear_remote_dir () {
  test_deploy_dir
  echo "emptying deploy dir"
  rm -rf $DEPLOY_DIR/*
}

deploy () {
  echo "deploying new build"
  cp -r $BUILD_DIR/* $DEPLOY_DIR || fail 5 "failed to copy files to deploy dir $DEPLOY_DIR"
}

unmount () {
  echo "unmounting deploy dir"
  sudo umount $DEPLOY_DIR || fail 6 "failed to unmount $DEPLOY_DIR"
}

print_options() {
  if [ $debug ]; then 
    echo "debug=$debug"
    echo "silent=$silent"
    echo "build=$build"
    echo "mount=$mount"
    echo "backup=$backup"
    echo "clear_deploy_dir=$clear_deploy_dir"
    echo "deploy=$deploy"
    echo "unmount=$unmount"
  fi
}

if [ $debug -gt 0 ]; then
  print_options
fi

if [ $build -gt 0 ]; then
  npm run build
fi

if [ $mount -gt 0 ]; then
  mount
fi

if [ $backup -gt 0 ]; then
  backup
fi

if [ $clear_deploy_dir -gt 0 ]; then
  clear_remote_dir
fi

if [ $deploy -gt 0 ]; then
  deploy
fi

if [ $unmount -gt 0 ]; then
  unmount
fi

