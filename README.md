# deployscript v 0.1

Copyright (c) 2019 Jon Johnson
Released under GPLv3, see LICENCE.txt

This is a bash script to simplify deployment of a web app with help of sshfs. Sshfs will mount a folder (the websites root folder) of a remote host (the web server) so that the content can be replaced with the contents of a local folder (e.g. ./build/).


